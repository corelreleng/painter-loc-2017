﻿/* Encoded in UTF-8 by Daniel Jetté*/

var gStringList = [
	['LOC_AboutLeft_01', 'Copyright &amp; rechtliche Hinweise'],
	['LOC_AboutLeft_02', 'Lizenzvertrag']
];

var gToolTipList = [
	['IconClose', 'Fenster schließen']
];

/*<IMPORTANT> Do not delete the loadComplete() at the bottom of this file */
loadComplete();
/*</IMPORTANT>*/