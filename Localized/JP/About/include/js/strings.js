﻿/* Encoded in UTF-8 by Daniel Jetté*/

var gStringList = [
	['LOC_AboutLeft_01', '著作権 &amp; および法的条項'],
	['LOC_AboutLeft_02', 'ライセンス契約']
];

var gToolTipList = [
	['IconClose', 'ウィンドウを閉じる']
];

/*<IMPORTANT> Do not delete the loadComplete() at the bottom of this file */
loadComplete();
/*</IMPORTANT>*/