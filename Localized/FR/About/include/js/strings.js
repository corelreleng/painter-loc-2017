﻿/* Encoded in UTF-8 by Daniel Jetté*/

var gStringList = [
	['LOC_AboutLeft_01', 'Mentions légales &amp; copyright'],
	['LOC_AboutLeft_02', 'Contrat de licence']
];

var gToolTipList = [
	['IconClose', 'Fermer la fenêtre']
];

/*<IMPORTANT> Do not delete the loadComplete() at the bottom of this file */
loadComplete();
/*</IMPORTANT>*/